import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ResidenceService } from "../main-lessee.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formResidenceAdd: FormGroup;

  constructor(private serviceResidence: ResidenceService, private router: Router) { }

  ngOnInit() {
    this.formResidenceAdd = new FormGroup({
      pcodeRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pnameRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      plocationRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      proomsRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pbedsRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pparLot: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pwifi: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }), 
      ppriceDay: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }), 
      pcontactRes: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }), 
      pcontactMail : new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }), 
    });
  }
  addResidence() {

    if (!this.formResidenceAdd.valid) {
      return;
    }

      this.serviceResidence.addResidence(
      this.formResidenceAdd.value.pcodeRes,
      this.formResidenceAdd.value.pnameRes,
      this.formResidenceAdd.value.plocationRes,
      this.formResidenceAdd.value.proomsRes,
      this.formResidenceAdd.value.pbedsRes,
      this.formResidenceAdd.value.pparLot,
      this.formResidenceAdd.value.pwifi,
      this.formResidenceAdd.value.ppriceDay,
      this.formResidenceAdd.value.pcontactRes,
      this.formResidenceAdd.value.pcontactMail,

    );

    this.formResidenceAdd.reset();
    
    this.router.navigate(["/main-lessee"]);
    
  }

}
