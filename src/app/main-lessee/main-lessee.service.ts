import { Injectable } from '@angular/core';
import { residence } from "./main-lessee.model";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResidenceService {
  private residence: residence[] = [
    {
      codeRes: 123,
      nameRes: "Casa Heredia",
      locationRes: "Heredia",
      roomsRes: 4,
      bedsRes: 8,
      parLot: "Yes",
      wifi: "Yes",
      priceDay: 100,
      contactRes: "Erick Munoz",
      contactMail: "erick@mail.com"
    },
  ];
  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    return [...this.residence];
  }

  getResidence(codeId: number) {
    return {
      ...this.residence.find((residence) => {
        return residence.codeRes === codeId;
      }),
    };
  }

  deleteResidence(codeId: number) {
    this.residence = this.residence.filter((residence) => {
      return residence.codeRes !== codeId;
    });
  }

  addResidence(
    pcodeRes: number,
    pnameRes: string,
    plocationRes: string,
    proomsRes: number,
    pbedsRes: number,
    pparLot: string,
    pwifi: string,
    ppriceDay: number,
    pcontactRes: string,
    pcontactMail: string,    
  ) {
    const residence: residence = {
      codeRes: pcodeRes,
      nameRes: pnameRes,
      locationRes: plocationRes,
      roomsRes: proomsRes,
      bedsRes: pbedsRes,
      parLot: pparLot,
      wifi: pwifi,
      priceDay: ppriceDay,
      contactRes: pcontactRes,
      contactMail: pcontactMail,
    }
     this.http.post('https://proyectofinal-47b7f-default-rtdb.firebaseio.com/addResidence.json',
        { 
           ...residence, 
           id:null
         }).subscribe(() => {
           console.log('entró');
         });

    this.residence.push(residence);
  }

  editResidence(
    pcodeRes: number,
    pnameRes: string,
    plocationRes: string,
    proomsRes: number,
    pbedsRes: number,
    pparLot: string,
    pwifi: string,
    ppriceDay: number,
    pcontactRes: string,
    pcontactMail: string,     
  ) {
    let index = this.residence.map((x) => x.codeRes).indexOf(pcodeRes);

    this.residence[index].codeRes = this.residence.map((x) => x.codeRes).indexOf(pcodeRes);
    this.residence[index].nameRes = pnameRes;
    this.residence[index].locationRes = plocationRes;
    this.residence[index].roomsRes = proomsRes;
    this.residence[index].bedsRes = pbedsRes;
    this.residence[index].parLot = pparLot;
    this.residence[index].wifi = pwifi;
    this.residence[index].priceDay = ppriceDay;
    this.residence[index].contactRes = pcontactRes;
    this.residence[index].contactMail = pcontactMail;

    console.log(this.residence);
  }
}