import { Component, Input, OnInit } from '@angular/core';
import { ResidenceService } from "./main-lessee.service";
import { residence } from "./main-lessee.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-main-lessee',
  templateUrl: './main-lessee.page.html',
  styleUrls: ['./main-lessee.page.scss'],
})
export class MainLesseePage implements OnInit {
  @Input() nameRes: string;
  residence: residence[];

  constructor(
    private ResidenceServices: ResidenceService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log("Carga inicial");
    this.residence = this.ResidenceServices.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.residence = this.ResidenceServices.getAll();
  }

  view(code: number) {
    this.router.navigate(["/main-lessee/view/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Residencia",
        message: "Esta seguro que desea borrar esta residencia?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.ResidenceServices.deleteResidence(code);
              this.residence = this.ResidenceServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  update(code: number) {
    this.router.navigate(["/main-lessee/edit/" + code]);
  }

}
