import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainLesseePage } from './main-lessee.page';

const routes: Routes = [
  {
    path: '',
    component: MainLesseePage
  },
  {
    path: 'add',
    loadChildren: () => import('./add/add.module').then( m => m.AddPageModule)
  },
  {
    path: 'edit/:codeId',
    loadChildren: () => import('./edit/edit.module').then( m => m.EditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainLesseePageRoutingModule {}
