import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MainLesseePage } from './main-lessee.page';

describe('MainLesseePage', () => {
  let component: MainLesseePage;
  let fixture: ComponentFixture<MainLesseePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainLesseePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MainLesseePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
