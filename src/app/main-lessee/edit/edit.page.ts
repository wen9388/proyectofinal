import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";
import { ResidenceService } from "../main-lessee.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { residence } from '../main-lessee.model';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  residence: residence;
  formResidenceEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceResidence: ResidenceService,
    private router: Router
  ) { }

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('codeId')) {
        return;
      }
      const codeId = parseInt(paramMap.get('codeId'));
      this.residence = this.serviceResidence.getResidence(codeId);
  });

  this.formResidenceEdit = new FormGroup({
    pcodeRes: new FormControl(this.residence.codeRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    pnameRes: new FormControl(this.residence.nameRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    plocationRes: new FormControl(this.residence.locationRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    proomsRes: new FormControl(this.residence.roomsRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    pbedsRes: new FormControl(this.residence.bedsRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    pparLot: new FormControl(this.residence.parLot, {
      updateOn: "blur",
      validators: [Validators.required],
    }),
    pwifi: new FormControl(this.residence.wifi, {
      updateOn: "blur",
      validators: [Validators.required],
    }), 
    ppriceDay: new FormControl(this.residence.priceDay, {
      updateOn: "blur",
      validators: [Validators.required],
    }), 
    pcontactRes: new FormControl(this.residence.contactRes, {
      updateOn: "blur",
      validators: [Validators.required],
    }), 
    pcontactMail : new FormControl(this.residence.contactMail, {
      updateOn: "blur",
      validators: [Validators.required],
    }), 
  });

  this.formResidenceEdit.value.pnombre = this.residence.nameRes;
}

editResidence() {

  if (!this.formResidenceEdit.valid) {
    return;
  }

    this.serviceResidence.addResidence(
    this.formResidenceEdit.value.pcodeRes,
    this.formResidenceEdit.value.pnameRes,
    this.formResidenceEdit.value.plocationRes,
    this.formResidenceEdit.value.proomsRes,
    this.formResidenceEdit.value.pbedsRes,
    this.formResidenceEdit.value.pparLot,
    this.formResidenceEdit.value.pwifi,
    this.formResidenceEdit.value.ppriceDay,
    this.formResidenceEdit.value.pcontactRes,
    this.formResidenceEdit.value.pcontactMail,

  );

  this.formResidenceEdit.reset();
  
  this.router.navigate(["/main-lessee"]);
  
}

}
