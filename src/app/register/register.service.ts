import { Injectable } from '@angular/core';
import { register } from "./register.model";
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private register: register[] = [
    {
      codeUser: 123,
      nameUser: "Erick Munoz",
      emailUser: "erick@mail.com",
      passUser: "admin",
      typeUser: "Cliente"
    },
  ];
  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    return [...this.register];
  }

  getRegister(codeId: number) {
    return {
      ...this.register.find((user) => {
        return user.codeUser === codeId;
      }),
    };
  }

  deleteRegister(codeId: number) {
    this.register = this.register.filter((user) => {
      return user.codeUser !== codeId;
    });
  }

  addRegister(
    pcodeUser: number,
    pnameUser: string,
    pemailUser: string,
    ppassUser: string,
    ptypeUser: string,
  ) {
    const register: register = {
      codeUser: pcodeUser,
      nameUser: pnameUser,
      emailUser: pemailUser,
      passUser: ppassUser,
      typeUser: ptypeUser,
      
    }
     this.http.post('https://proyectofinal-47b7f-default-rtdb.firebaseio.com/addRegister.json',
        { 
           ...register, 
           id:null
         }).subscribe(() => {
           console.log('entró');
         });

    this.register.push(register);
  }
}
