import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, MinLengthValidator, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { RegisterService } from "../register.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formRegisterAdd: FormGroup;

  constructor(private serviceRegister: RegisterService, private router: Router) { }

  ngOnInit() {
    this.formRegisterAdd = new FormGroup({
      pcodeUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.min(1),],
      }),
      pnameUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pemailUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ppassUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ptypeUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }

  addRegister(){
    if (!this.formRegisterAdd.valid){
      return;
    }
    this.serviceRegister.addRegister(
    this.formRegisterAdd.value.pcodeUser,
    this.formRegisterAdd.value.pnameUser,
    this.formRegisterAdd.value.pemailUser,
    this.formRegisterAdd.value.ppassUser,
    this.formRegisterAdd.value.ptypeUser,
    );

    this.formRegisterAdd.reset();

    this.router.navigate(["/register"]);
  }

}
