import { Component, Input, OnInit } from '@angular/core';
import { RegisterService } from "./register.service";
import { register } from "./register.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @Input() nameUser: string;
  register: register[];

  constructor(
    private RegisterServices: RegisterService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
      console.log("Carga inicial");
      this.register = this.RegisterServices.getAll();
    }
    
    ionViewWillEnter() {
      console.log(this.register);
      this.register = this.RegisterServices.getAll();
    }
  
    view(code: number) {
      this.router.navigate(["/main-user/view/" + code]);
    }
  
    update(code: number) {
      console.log(this.register);
      this.router.navigate(["/main-user/edit/" + code]);
    }

}
