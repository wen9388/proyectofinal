// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // Login Configuration
firebaseConfig : {
  apiKey: "AIzaSyDCmGAOvGiAOmBAbd01XF3xX6p8omUBXZs",
  authDomain: "proyectofinal-47b7f.firebaseapp.com",
  databaseURL: "https://proyectofinal-47b7f-default-rtdb.firebaseio.com",
  projectId: "proyectofinal-47b7f",
  storageBucket: "proyectofinal-47b7f.appspot.com",
  messagingSenderId: "667570594190",
  appId: "1:667570594190:web:ba90141810137cd720c4df",
  measurementId: "G-2L1G0VL4EC"
},
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
